# AMP Neos base components

This package contains all built-in AMP components.

## Components

AMP Component | Fusion Component
--- | ---
`amp-img` [docs](https://www.ampproject.org/docs/reference/components/amp-img) | `Yroot.Amp.Base:Image` 
`amp-pixel` [docs](https://www.ampproject.org/docs/reference/components/amp-pixel)  | `Yroot.Amp.Base:Pixel` 
`amp-layout` [docs](https://www.ampproject.org/docs/reference/components/amp-layout) | `Yroot.Amp.Base:Layout` 